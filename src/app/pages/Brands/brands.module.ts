import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgaModule } from '../../theme/nga.module';
import { brands } from './brands.component';
import { tableComponent } from './table/table.component';

import { routing } from './brands.routing';

import { SmartTablesService } from './table/smartTables.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    routing,
    Ng2SmartTableModule,
    NgaModule
  ],
  declarations: [
    brands,
     tableComponent
  ],
  providers: [
   SmartTablesService 

  ]
  
})
export class brandsModule {}