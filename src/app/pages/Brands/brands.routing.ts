import { Routes, RouterModule } from '@angular/router';

import { brands } from './brands.component';
import { tableComponent } from './table/table.component';

const routes: Routes = [
  {
    path: '',
    component: brands,
    children: [
      { path: 'table', component: tableComponent },

    ]
  }
];

export const routing = RouterModule.forChild(routes);