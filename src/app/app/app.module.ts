import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { DataService } from './Services/data.service';

import { AppComponent } from './App-component/app.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { BodyDefaultComponent } from './body-default/body-default.component';
import { RegisterComponent } from './register/register.component';
import { AppRoutingModule }     from './app-routing.module';
import { CategoryComponent } from './category/category.component';

import { ProductDetailsComponent } from './product-details/product-details.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { PublicComponent } from './App-component/public/public.component';
import { SecureComponent } from './App-component/secure/secure.component';
import { SubCategoryComponent } from './sub-category/sub-category.component';
import { ProductsComponent } from './products/products.component';
import { CartComponent } from './cart/cart.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    BodyDefaultComponent,
    RegisterComponent,
    CategoryComponent,
    ProductDetailsComponent,
    DashboardComponent,
    PublicComponent,
    SecureComponent,
    SubCategoryComponent,
    ProductsComponent,
    CartComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModules { }
