import { Component, OnInit } from '@angular/core';
import { DataService } from '../Services/data.service';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-sub-category',
  templateUrl: './sub-category.component.html',
  styleUrls: ['./sub-category.component.css']
})
export class SubCategoryComponent implements OnInit {

 subcat;
 id;
   

  constructor(private DataService: DataService,private activatedRoute: ActivatedRoute) { }

 

  ngOnInit() {

    this.activatedRoute.params.subscribe((params: Params) => {
        this.id = +params['id']; });
      

     this.DataService.getSubCategoryByCategoryId(this.id).subscribe(response => {

      console.log(response.Status);

      if(response.Status != null)
      {
        console.log("done");
        switch(response.Status)
        {
          case "Success":
          this.subcat = response.data;
          console.log(this.subcat);
          break;
          case "Failure":
          console.log(this.subcat+" Fail");
          break;
        }
      }
  });
  }
}
