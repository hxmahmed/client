import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

  constructor(private http: Http) { }


      getCategory()
    {
            let url = 'http://localhost:3000/api/category/';    
            return this.http.get(url,new Headers ({'Content-Type':'application/json'})).map(response => response.json());      
    }

         getSubCategory()
    {
            let url = 'http://localhost:3000/api/sub_category/';    
            return this.http.get(url,new Headers ({'Content-Type':'application/json'})).map(response => response.json());      
    }

         getSubCategoryByCategoryId(id)
    {
            let url = 'http://localhost:3000/api/sub_category/searchByCategoryId/'+id;    
            return this.http.get(url,new Headers ({'Content-Type':'application/json'})).map(response => response.json());      
    }


             getproductsBySubCategoryId(id)
    {
            let url = 'http://localhost:3000/api/product/searchBySubCategoryId/'+id;    
            return this.http.get(url,new Headers ({'Content-Type':'application/json'})).map(response => response.json());      
    }

}
