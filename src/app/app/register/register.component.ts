import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { registerService } from '../Services/register.Service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
   providers: [registerService]
})
export class RegisterComponent implements OnInit {

  constructor(private registerService: registerService) { }

  response;

  ngOnInit() {
  }


  SignUser(form : NgForm) : void
  {
    this.registerService.RegisterRequest(form.value).subscribe(response => {


      if(response.Status != null)
      {
        console.log("done");
        switch(response.Status)
        {
          case "Success":
         

          break;
          case "Failure":
          this.response = response.Message;
          break;
        }
      }
  });

   }
  }



