import { Component, OnInit , EventEmitter, Input, Output } from '@angular/core';
import { loginService } from '../Services/Login.Service';
import { NgForm } from '@angular/forms';
import { Cookie } from 'ng2-cookies/ng2-cookies';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],
  providers: [loginService]
})
export class LoginFormComponent implements OnInit {
  
  response;
   @Output() log = new EventEmitter<string>();

  constructor(private loginService: loginService) { }

  ngOnInit() {
    
  }

 loginUser(form : NgForm) : void
  {
    
   
    this.loginService.loginRequest(form.value).subscribe(response => {


      if(response.Status != null)
      {
        console.log("done");
        switch(response.Status)
        {
          case "Success":
          Cookie.set("token", response.data);
          Cookie.set("id", response.Message.id);
          Cookie.set("Username", response.Message.Username);
          Cookie.set("first_name", response.Message.first_name);
          Cookie.set("UserType", response.Message.UserType.name);
          location.reload();
          

          break;
          case "Failure":
          this.response = response.Message;
          break;
        }
      }
  });

   }

}
