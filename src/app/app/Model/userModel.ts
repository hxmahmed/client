export class User
{
    fname : string;
    lname : string
    uname : string;
    email : string;
    contact : number;
    city : string;
    country : string;
    address : string;
    password : string;
    rpassword : string
}