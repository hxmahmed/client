import { Component, OnInit } from '@angular/core';
import { DataService } from '../Services/data.service';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

totalproducts;
 products;
 id;
   

  constructor(private DataService: DataService,private activatedRoute: ActivatedRoute) { }

 

  ngOnInit() {

    this.activatedRoute.params.subscribe((params: Params) => {
        this.id = params['id']; });

     this.DataService.getproductsBySubCategoryId(this.id).subscribe(response => {

      console.log(response.Status);

      if(response.Status != null)
      {
        console.log("done");
        switch(response.Status)
        {
          case "Success":
          this.products = response.data;
          this.totalproducts = this.products.length;
          console.log(response.data+"success");
          break;
          case "Failure":
          console.log(this.products+" Fail");
          break;
        }
      }
  });
  }
}
