import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import{PublicComponent} from './App-component/public/public.component';
import{SecureComponent} from './App-component/secure/secure.component';

import{PUBLIC_ROUTES} from './App-component/public/public.routes';
import{SECURE_ROUTES} from './App-component/secure/secure.routes';

// const routes: Routes = [
//   { path: '', redirectTo: '/home', pathMatch: 'full' },
//   { path: 'login',  component: LoginFormComponent },
//   { path: 'register',  component: RegisterComponent },
//   { path: 'home', component: BodyDefaultComponent },
//   {path:'details.html', component:CategoryComponent },
//   {path:'detailss.html',component:CategoryTypeComponent },
//   {path:'d.html',component:ProductDetailsComponent},
//   {path:'j.html',component:BuyingDetailsComponent},
//   {path:'dashboard',component:DashboardComponent}
// ];


const APP_ROUTES: Routes = [
   // { path: '', redirectTo: '/home', pathMatch: 'full', },
    { path: '', component: PublicComponent, data: { title: 'Public Views' }, children: PUBLIC_ROUTES },
   // { path: '', component: SecureComponent, data: { title: 'Secure Views' }, children: SECURE_ROUTES }
];

@NgModule({
  imports: [ RouterModule.forRoot(APP_ROUTES) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}