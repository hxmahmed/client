import { Component , OnInit} from '@angular/core';
import { loginService } from '../Services/Login.Service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [loginService]
})
export class AppComponent {
  title = 'app works!';
  hover;



 getuser:string="login";

  constructor(private loginService: loginService) { }

   

   ngOnInit() {

     this.getuser = "Login"+loginService.loggedin;
  }

  log(user:string)
  {
      this.getuser = user;
  }

  show() : string
  {
    if(this.hover == true)
    {
      return "block";
    }
    else
    {
       return "none";
    }
   

  }

  hide(opt:string)
  {
    if(opt=='body')
    {
      this.hover=false;
    }
    if(opt='login')
    {
      this.hover=true;
    }
  }
}
