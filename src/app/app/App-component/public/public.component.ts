import { Component, OnInit } from '@angular/core';
import { DataService } from '../../Services/data.service';
import {Router, ActivatedRoute, Params} from '@angular/router';

import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.css']
})
export class PublicComponent implements OnInit {

 cat;
   sub;
id;

username = Cookie.get('first_name');
  constructor(private DataService: DataService,private activatedRoute: ActivatedRoute, private router:Router) { 


  }

 

  ngOnInit() {

    this.username = Cookie.get('first_name');

     this.sub = this.activatedRoute.params.subscribe((params: Params) => {
        this.id = params['id']; });

    //    this.router.navigate(['/sub-category',this.id]);

     this.DataService.getCategory().subscribe(response => {

      console.log(response.Status);

      if(response.Status != null)
      {
        console.log("done");
        switch(response.Status)
        {
          case "Success":
          this.cat = response.data;
          console.log(this.cat);
          break;
          case "Failure":
          console.log(this.cat+" Fail");
          break;
        }
      }
  });
}

ngOnDestroy() {
    this.sub.unsubscribe();
}



}
