import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginFormComponent} from '../../login-form/login-form.component';
import {BodyDefaultComponent} from '../../body-default/body-default.component';
import {RegisterComponent} from '../../register/register.component';
import {CategoryComponent} from '../../category/category.component';
import {SubCategoryComponent} from '../../sub-category/sub-category.component';
import {ProductsComponent} from '../../products/products.component';
import{ProductDetailsComponent} from '../../product-details/product-details.component';
import{CartComponent} from '../../cart/cart.component';
import{DashboardComponent} from '../../dashboard/dashboard.component';


export const PUBLIC_ROUTES: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'login',  component: LoginFormComponent },
  {path: 'register',  component: RegisterComponent },
  {path: 'home', component: BodyDefaultComponent },
  {path:'category', component:CategoryComponent },
  {path:'sub-category/:id',component:SubCategoryComponent },
  {path:'products/:id',component:ProductsComponent },
  {path:'product-details',component:ProductDetailsComponent},
  {path:'cart',component:CartComponent}
];