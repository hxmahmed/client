import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import{DashboardComponent} from '../../dashboard/dashboard.component';

export const SECURE_ROUTES: Routes = [
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    {path:'dashboard',component:DashboardComponent}
  //  { path: 'items', component: ItemsComponent },

];